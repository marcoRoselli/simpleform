<?php defined('constCheck') or die ("Restricted access");?>

<form method="post" id="formId" action="index.php?task=saveData">
	<!--SECTION 1-->
	<div id="section1" class="section">
		<h3>Step1: your details</h3>
		<div class="sectionContent">
			<div class="row">
				<!--FIRST NAME-->
				<div class="field" >
					<div class="label">First Name</div>
					<div class="value">
						<input type="text" name="FName" placeholder="Your first name here" required="required" validation="text" />
					</div>
				</div>
				<!--LAST NAME-->
				<div class="field" >
					<div class="label">Surname</div>
					<div class="value">
						<input type="text" name="LName" placeholder="Your last name here" required="required" validation="text" />
					</div>
				</div>
			</div>
				
			<div class="row">
				<!--EMAIL-->
				<div class="field">
					<div class="label">	Email adress</div>
					<div class="value">
						<input type="email" name="email" placeholder="Your email address here" required="required" validation="email" />
					</div>
				</div>		
				<!--BUTTON-->	
				<div id="button1"class="button">
					Next >
				</div>
			</div>
		</div>
	</div>
	
	<!--SECTION 2-->
	<div id="section2" class="section">
		<h3>Step2: some more details</h3>
		<div class="sectionContent hide">
			<div class="row">
				<!--TELEPHONE NUMBER-->
				<div class="field">
					<div class="label">	Telephone number</div>
					<div class="value">
						<input type="tel" name="telephone" placeholder="Your telephone number here" required="required" validation="telephone" />
					</div>
				</div>
				<!--DOB-->
				<div class="field">
					<div class="label">	Date of birth (DD/MM/YYYY)</div>
					<div class="value">
						<input type="date" name="dob" required="required" validation="date" placeholder="Your date of birth here">
					</div>
				</div>
			</div>
			<div class="row">
				<!--GENDER-->
				<div class="field">
					<div class="label">	Gender</div>
					<div class="value">
						<input type="radio" name="gender" value="M" checked >Male
						<input type="radio" name="gender" value="F">Female
						<input type="radio" name="gender" value="O">Other
					</div>
				</div>
				<!--BUTTON-->	
				<div id="button2" class="button">
					Next >
				</div>
			</div>
		</div>
	</div>
	
	<!--SECTION 3-->
	<div id="section3" class="section">
		<h3>Step3: comments</h3>
		<div class="sectionContent hide">
			<div class="row">
				<!--COMMENTS-->
				<div class="field">
					<div class="label">	Comments (max 500 chars)</div>
					<div class="value">
						<textarea type="comments" name="comments" cols="40" rows="10" validation="comments" placeholder="Your comments here" ></textarea>
					</div>
				</div>
				<!--SUBMIT-->
				<div id="buttonSubmit" class="button">
					<input type="submit" name="submitButton" id="submitButton" value="Save data" />
				</div>
			</div>
		</div>				
	</div>
	
</form>
