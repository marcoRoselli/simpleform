<?php

class simpleForm {
	
	function __construct() {
		include_once('configuration.php');
		$this->host = $host;
		$this->dbName = $dbName;
		$this->username = $username;
		$this->password = $password;
	}
	
	private function connectDb() {		
		$dbConn = new mysqli($this->host,$this->username,$this->password);
		if ($dbConn->connect_error) {
			$error = 'Unable to connect to server';
			throw new Exception ($error);
		}
		return ($dbConn);
	}
		
			
	/**
	 * @desc:save data in DB
	 * @params: sanited array of values
	 **/
	function saveData($unsValues) {
		$post = self::validate($unsValues);
		$error = false;
		$result = true;
		$fieldsArr = ["FName","LName","email","telephone","dob","gender","comments"];
		try {
			$dbConn = self::connectDb();
		} catch (Exception $e) { return json_encode([false,$e->getMessage()]);
		}
		
		//create db or select it if already existent
		$dbCheck = $dbConn->select_db('entryForm');
		if (!$dbCheck) {
			try {	
				$query = 'CREATE DATABASE IF NOT EXISTS entryForm';
				//check if creation ok
				if (!$dbConn->query($query)) {
					throw new Exception ('Unable to create database');
				}
			} catch (Exception $e) { return json_encode([false,$e->getMessage()]);
			}
		$dbConn->select_db('entryForm');
		}
		
		//create table if doesnt exists
		$checkTable = $dbConn->query("SELECT * FROM information_schema WHERE TABLE_NAME='user'");
			
		if (!$checkTable) {
			try {
				$dbConn->query('CREATE TABLE IF NOT EXISTS user(
					id INT(6) NOT NULL AUTO_INCREMENT PRIMARY KEY,
					FName varchar(60),
					LName varchar (60),
					email varchar(30),
					telephone varchar (30),
					gender varchar(1),
					dob varchar(30),
					comments varchar(500)
					)');
				if ($dbConn->connect_error) {
					$error =  'Unable to create database';
					throw new Exception ($error);
				}
			}	catch (Exception $e) {return json_encode([false,$e->getMessage()]);
			}
		}	
		
		$fieldsString = implode(',',$fieldsArr);

		
		$query = "INSERT INTO user (". $fieldsString .") VALUES (?,?,?,?,?,?,?)";		
		try {
			$stmt = $dbConn->stmt_init();
			if (!$stmt->prepare($query) ){
				throw new Exception ('Unable to prepare query, '.$stmt->error);
			}
			$stmt->bind_param('sssssss',$post['Fname'],$post['LName'],$post['email'],$post['telephone'],$post['dob'],$post['gender'],$post['comments']);
		} catch (Exception $e) {return json_encode([false,$e->getMessage()]);
		}
		try {
			if (!$stmt->execute()) {
				throw new Exception("Unable to execute creation query");
			}
		} catch (Exception $e) {return json_encode([false,$e->getMessage()]);
		} 
		$stmt->close();
		$dbConn->close();
		return(json_encode($post));
	}
	
	/**
	 *@desc: retrieve all records from DB
	 *@return: (json array)
	 */
	function showAll() {
		try {
			$dbConn = self::connectDb();
		} catch (Exception $e) { return json_encode([false,$e->getMessage()]);
		}
		try {
			if(!$dbConn->select_db('entryForm')) {
				throw new Exception ("Unable to get the database table");
			}
		} catch (Exception $e) { return json_encode([false,$e->getMessage()]);
		}
		try {
			$query = 'SELECT * from user ORDER BY id ASC';
			$result = $dbConn->query($query);	
			if ($result->num_rows < 1) {
				throw new Exception ("Unable to retrieve any data from the database");
			}
		} catch (Exception $e) { return json_encode([false,$e->getMessage()]);
		}
		$dbConn->close();
		return ($result);
	}
	
	
			
	private function validate($values) {
		$cleanVal = [];
		foreach ($values as $field=>$value) {
			switch ($field){
				case ('email'):
					$cleanVal[$field] = filter_var($value,FILTER_SANITIZE_EMAIL);
				break;
				default: 
					$cleanVal[$field] = filter_var($value,FILTER_SANITIZE_STRING);
				break;
			}
		}
		return ($cleanVal);
	}
}		



