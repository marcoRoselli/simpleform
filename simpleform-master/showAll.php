
<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<meta name="viewport" content="device-width;initial-scale=1">
		<meta charset="UTF-8"
		<meta name="description" content="Show all files">
		<meta name="author" content="Marco Roselli">
		<link rel="stylesheet" type="text/css" href="entryForm.css">
		<title>Show all records</title>

	</head>
	<body>
		<div id="main_container">
			<?php

			$dbConn = new mySqli('localhost','maxximo','111111');
			if ($dbConn->connect_error) {
				echo "Unable to connect to server";
			}

			if(!$dbConn->select_db('entryForm')) {
				echo "Unable to get the database table";
			}

			$query = 'SELECT * from user ORDER BY id ASC';
			$result = $dbConn->query($query);	

			if ($result->num_rows > 0 ) {	?>
			<table>	<?php
				$firstRow = $result->fetch_assoc();	?>
				<tr>	<?php
				foreach ($firstRow as $field=>$value) {	?>	
					<th>	<?php
						echo $field;	?>
					</th>	<?php
				}	?>
				</tr>
				<tr>	<?php
				foreach ($firstRow as $field=>$value) {	?>	
					<td>	<?php
						echo $value;	?>
					</td>	<?php
				}	?>
				</tr>	<?php
				
				while ($row = $result->fetch_assoc()) {?> 	   
					<tr>	<?php
					foreach ($row as $field=>$value) {	?>
						<td>	<?php
						echo $value;	?>
						</td>	<?php
					}	?>
					</tr> 	<?php
				
				}	?>
			</table>	<?php
			} else {
				echo "Unable to get any record from the table";
			}

			$dbConn->close();	?>
		
			<a href="index.php"><div class="button back">Back to form</div></a>
		</div>	
		
	</body>
</html>


	




