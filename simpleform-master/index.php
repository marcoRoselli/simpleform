<?php
/**
 * Entry form to get some data from users
**/
?>
<?php
include_once('processor.php');
$simpleForm = new simpleForm();
define (constCheck,TRUE);

if ($_GET['task'] && $_GET['task'] == "saveData") {
	$result = $simpleForm->saveData($_POST);
	echo ($result);
	
} else {	?>
	<!DOCTYPE html>
	<html lang="en-GB">
		<head>
			<meta name="viewport" content="device-width;initial-scale=1">
			<meta charset="UTF-8"
			<meta name="description" content="A simple form to get some data from users">
			<meta name="author" content="Marco Roselli">
			<link rel="stylesheet" type="text/css" href="entryForm.css">
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
			<script src="entryForm.js"></script>
			<title>A simple registration form</title>

		</head>
		
		<body>
		<div id ="main_container"> <?php
		switch ($_GET['task']) {
			case ('confirmation'):
				require_once ('layout/confirmation.php');
			break;
			case ('showAll'):
				$result = $simpleForm->showAll();
				require_once ('layout/showAll.php');
			break;
			default:
				require_once('layout/bodyForm.php');
			break;
		}	?>
		</div>	
	</body>


	</html>	<?php
}
	
