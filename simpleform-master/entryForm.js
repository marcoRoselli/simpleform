
function entryForm () {
		//set timer for input validation
		var timer;
		var typingInterval = 2000;
		
		//get all form element
		var formData = document.getElementById('formId');
		//prevent html validation here, will be done later 
		formData.noValidate = true;
		var elements = formData.elements;
		//run validation if element has validation tag
		for (var k=0; k < elements.length; k++) {
			if (elements[k].hasAttribute('validation') == true ) {
				elements[k].addEventListener('keyup',function(e) {
					clearTimeout(timer);
					if (e.target.value !== null) {
						timer = setTimeout(function () {
							validate(e.target);
						},typingInterval);
					}
				},false);
				elements[k].addEventListener('click',removeError,false);
				elements[k].addEventListener('keydown',removeError,false);
			}			
		}
		//add listener to form submit to validate all fields before sending ajax request 
		formData.addEventListener('submit',function(e){
			e.preventDefault();
			checkValidations(e);
		},false);
		

	/**
	 * Validate all fields again at submit, if ok send ajax to update DB
	 **/
	function checkValidations(e) {
		var error = false;
		for (var k=0; k < elements.length; k++) {
			if (elements[k].hasAttribute('validation')) {
				validate(elements[k]);
				if ( (elements[k].hasAttribute('validated') === false) || (elements[k].attributes.validated.value === "false") ) {
					error = true;
					$(elements[k]).parents('.section').show();
				}
			}
		}
		if (error === false) {
			preparePostData(elements);		
		}
	}
	/**
	 * Prepare validated data to be sent to ajax
	 **/
	function preparePostData(elements) {console.log("prepare post data");
		var data = {};
		for (var k=0; k < elements.length; k++) {
			var field = elements[k].attributes.name.value;
			data[field] = elements[k].value;
		}
		strToPost = [];
		for (var prop in data) {
			strToPost += "&"+prop+"="+data[prop]; 	
		}
		//send ajax request
		xhrRequest(formData.action,formData.method,strToPost,function(response) {
				checkConfirmation(response);
		});
		
		function checkConfirmation(response) {
			var messageSpace = document.getElementById('messageSpace');
			if (messageSpace == null) {
				var messageSpace = document.createElement('div');
				messageSpace.setAttribute('id','messageSpace');
				formData.parentNode.insertBefore(messageSpace,formData);
			}
			response = JSON.parse(response);
			if (response[0] !== false) {
				var vals = $.param(response);
				window.location.href = "index.php?task=confirmation";
			} else {
					messageSpace.innerHTML = "Data not uploaded <br />";
					messageSpace.innerHTML += response[1];
					document.getElementById('section1').getElementsByClassName('sectionContent')[0].style.display = "";
			}
		}
				
			
				
	}
	
	/**JS function to submit ajax request
	*calls callback function if succesfully received outcome
	*expect to receive JSON data
	**/
	function xhrRequest(url,method,strToPost,callback){
		if (window.XMLHttpRequest) {
		var xhr = new XMLHttpRequest();
		} else {
		var xhr = new ActiveXObject('Microsoft.XMLHTTP');
		}
		 
		xhr.onreadystatechange = function () {
			if (xhr.status == "200" && xhr.readyState == "4") {
				callback.call(undefined,xhr.response);
			}
			if (xhr.status == "500" && xhr.readyState == "4") {
				alert ("Unable to process the request");
			}
		}
		xhr.open(method,url,true);
		xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
		xhr.send(strToPost);
	} 
	
	
	/**
	 * remove input error message at key down on field
	 **/
	function removeError(e) {
		var errors = e.target.parentNode.getElementsByClassName('error');
		if (errors !== null) {
			for(var z=0; z < errors.length;z++){
				e.target.parentNode.removeChild(errors[z]);
			}
		}
	}
				
	
}

/**
 * validate fields and add send error message if input is not ok
 **/
function validate(element) {
	var validation = true;
	var type = element.attributes.validation.value; 
	switch (type){
		case 'text': 
			var reg = new RegExp("^[a-zA-Z0-9. ]*$");
			if (element.value.length === 0) {
				errorMessage = "This field is mandatory";
				validation = false;
			}
			if (reg.test(element.value) == false) {
				errorMessage = "Some characters are not allowed";
				validation = false;
			}
		break;
		case 'email':
			element.setAttribute('validated','false');
			var reg = new RegExp("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$");
			if (element.value.length === 0) {
				errorMessage = "This field is mandatory";
				validation = false;
			}
			if (reg.test(element.value) == false) {
				errorMessage = "Please insert a valid email address";
				validation = false;
			}
		break;
		case 'telephone': 
			element.setAttribute('validated','false');
			var reg = new RegExp("^[0-9]*$");
			if (element.value.length === 0) {
				errorMessage = "This field is mandatory";
				validation = false;
			}
			if (reg.test(element.value) == false) {
				errorMessage = "Please insert a valid telephone number";
				validation = false;
			}
		break;
		case 'date': 
			element.setAttribute('validated','false');
			var reg = new RegExp("^[0-9 ]{2}/[0-9 ]{2}/[0-9]{4}$");
			if (element.value.length === 0) {
				errorMessage = "This field is mandatory";
				validation = false;
			}
			if (reg.test(element.value) == false) {
				errorMessage = "Please insert date in the requested format";
				validation = false;
			} else {
				var parts = element.value.split("/");
				var year = new Date().getFullYear();
				if (parts[0]>31 || parts[1]<1 || parts[1]>12 || parts[2]>year){
					errorMessage = "Some values of your date are not correct";
					validation = false;
				}
			}
		break;				
		case 'comments': 
			var reg = new RegExp("^[a-zA-Z0-9. ]*$");
			if (reg.test(element.value) == false) {
				errorMessage = "Some characters are not allowed";
				validation = false;
			}
		break;
	}	
	if (validation === false ) {
		element.setAttribute('validated','false');
		var message = element.parentNode.getElementsByClassName('error');
		if ( message.length === 0) {
			var message = document.createElement('div');
			message.setAttribute('class','error');
			element.parentNode.appendChild(message);
		}
		message.innerHTML = errorMessage;
	}	else {
			element.setAttribute('validated','true');
	}			
}
				
document.addEventListener('DOMContentLoaded',entryForm,false);

/**
 * @desc: switch the panels of the registration form once a section has been completed
 * @using jQuery just for exercise
**/
$(document).ready(function() {
	var sectionContents = $('.sectionContent');
	$('.button').each(function() {
		//validate fields of each section before showing the following
		$(this).click(function() {
			var error = false;
			var inputs = $(this).parents('.sectionContent').find('input');
			$.each(inputs,function(){
				var input = $(this);
				//check if field is supposed to be validated
				if ( $(this).attr('validation')!== undefined && $(this).attr('validation') !== false ) {
					//validated if required
					validate(this);
				//check if the validation was done and succesful
					if (input.attr('validated') === "false" || input.attr('validated') === undefined || input.attr('validated') === false ) {
						error = true;
					}
				}
			});
		if (error === false) {
			//if all fields are ok show the next section
			$(this).parents('.section').find('.sectionContent').slideUp(400);
			$(this).parents('.section').next().find('.sectionContent').slideDown(600);
		}	
		});
	})
});


